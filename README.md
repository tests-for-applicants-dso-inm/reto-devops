# Reto Aspirantes DevSecOps

Materiales:

-   Aplicación NodeJS ./app
-   URL: <https://gitlab.com/tests-for-applicants-dso-inm/reto-devops> 

---------------------------------------------------------------------------------------------------------------------------------------------

Crear Dockerfile para la aplicación

Requisitos:

-   Se debe crear un repositorio en gitlab
-   Se debe subir el Dockerfile y la aplicación NodeJS al repositorio
-   Al ejecutar el contenedor debe levantar en el puerto 3000

-   ***Entrega***: Url del repositorio (público) en donde se pueda ver el Dockerfile creado

---------------------------------------------------------------------------------------------------------------------------------------------

Crear pipeline para gitlab-ci

Requisitos:

-   Debe ser funcional
-   Debe tener los siguientes stages:

1. Build: Debe construir la imagen de docker creada para la aplicación NodeJS
2. Build: Se debe versionar la imagen en container registry del repositorio
3. Test: Debe ejecutar los test unitarios de la aplicación

-   ***Entrega***: Url al pipeline gitlab-ci.yml

Opcional: Puede levantar un gitlab-runner local para realizar la ejecución del pipeline

Documentación: <https://docs.gitlab.com/runner/install/>
